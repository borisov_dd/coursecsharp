﻿using System;

namespace Ex1_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[1000];
            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {                
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum += i;
                }
            }
            Console.WriteLine(sum);
        }
    }
}
